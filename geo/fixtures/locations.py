import os
import csv
from geo.models import Location, Country

__author__ = 'Francois CONSTANT'


def load_locations(fast=False):
    australia, created = Country.objects.get_or_create(
        name="Australia",
        code="au"
    )

    csv_path = os.path.join(os.path.dirname(__file__), "files", "geo_locations.csv")

    with open(csv_path, 'rU') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=',', quotechar='"')

        index = 1
        for row in spamreader:
            if fast and index >= 100:
                break

            location = _raw_column_to_location(row, australia)
            index += 1


def _raw_column_to_location(row, australia):
    #id,country_id,region_id,name,state,keywords,point
    ID_INDEX = 0
    COUNTRY_ID_INDEX = 1
    REGION_ID_INDEX = 2
    NAME_INDEX = 3
    STATE_INDEX = 4
    POSTCODE_INDEX = 5
    KEYWORDS_INDEX = 5
    POINT_INDEX = 6

    return Location.objects.create(
        country = australia,
        city = None,
        name = row[NAME_INDEX],
        state = row[STATE_INDEX],
        postcode = row[POSTCODE_INDEX].split(' ').pop(),
        keywords = ' '.join([row[NAME_INDEX], row[STATE_INDEX], row[KEYWORDS_INDEX]]),
        point = row[POINT_INDEX],
        rank = 0
    )
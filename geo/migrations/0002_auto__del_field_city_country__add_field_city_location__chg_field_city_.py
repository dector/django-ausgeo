# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting field 'City.country'
        db.delete_column(u'geo_city', 'country_id')

        # Adding field 'City.location'
        db.add_column(u'geo_city', 'location',
                      self.gf('django.db.models.fields.related.ForeignKey')(related_name='+', null=True, to=orm['geo.Location']),
                      keep_default=False)


        # Changing field 'City.slug'
        db.alter_column(u'geo_city', 'slug', self.gf('autoslug.fields.AutoSlugField')(unique=True, max_length=50, populate_from=('name',), unique_with=()))
        # Adding index on 'City', fields ['slug']
        db.create_index(u'geo_city', ['slug'])


    def backwards(self, orm):
        # Removing index on 'City', fields ['slug']
        db.delete_index(u'geo_city', ['slug'])

        # Adding field 'City.country'
        db.add_column(u'geo_city', 'country',
                      self.gf('django.db.models.fields.related.ForeignKey')(default=1, related_name='cities', to=orm['geo.Country']),
                      keep_default=False)

        # Deleting field 'City.location'
        db.delete_column(u'geo_city', 'location_id')


        # Changing field 'City.slug'
        db.alter_column(u'geo_city', 'slug', self.gf('autoslug.AutoSlugField')(max_length=50, unique=True, populate_from=('name',)))

    models = {
        u'geo.city': {
            'Meta': {'object_name': 'City'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'location': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'+'", 'null': 'True', 'to': u"orm['geo.Location']"}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'slug': ('autoslug.fields.AutoSlugField', [], {'unique': 'True', 'max_length': '50', 'populate_from': "('name',)", 'unique_with': '()'})
        },
        u'geo.country': {
            'Meta': {'object_name': 'Country'},
            'code': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '2'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'geo.location': {
            'Meta': {'ordering': "('name',)", 'object_name': 'Location'},
            'city': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'locations'", 'null': 'True', 'to': u"orm['geo.City']"}),
            'country': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'locations'", 'to': u"orm['geo.Country']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'keywords': ('django.db.models.fields.CharField', [], {'max_length': '1024', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'point': ('location_field.models.LocationField', [], {'null': 'True', 'blank': 'True'}),
            'postcode': ('localflavor.au.models.AUPostCodeField', [], {'max_length': '4'}),
            'rank': ('django.db.models.fields.IntegerField', [], {'default': '0', 'db_index': 'True'}),
            'state': ('localflavor.au.models.AUStateField', [], {'max_length': '3'})
        }
    }

    complete_apps = ['geo']
# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Country'
        db.create_table(u'geo_country', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('code', self.gf('django.db.models.fields.CharField')(unique=True, max_length=2)),
        ))
        db.send_create_signal(u'geo', ['Country'])

        # Adding model 'City'
        db.create_table(u'geo_city', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('country', self.gf('django.db.models.fields.related.ForeignKey')(related_name='cities', to=orm['geo.Country'])),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('slug', self.gf('autoslug.AutoSlugField')(populate_from=('name',), unique=True, max_length=50, db_index=False, blank=True)),
        ))
        db.send_create_signal(u'geo', ['City'])

        # Adding model 'Location'
        db.create_table(u'geo_location', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('country', self.gf('django.db.models.fields.related.ForeignKey')(related_name='locations', to=orm['geo.Country'])),
            ('city', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='locations', null=True, to=orm['geo.City'])),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('state', self.gf('localflavor.au.models.AUStateField')(max_length=3)),
            ('postcode', self.gf('localflavor.au.models.AUPostCodeField')(max_length=4)),
            ('keywords', self.gf('django.db.models.fields.CharField')(max_length=1024, blank=True)),
            ('point', self.gf('location_field.models.LocationField')(null=True, blank=True)),
            ('rank', self.gf('django.db.models.fields.IntegerField')(default=0, db_index=True)),
        ))
        db.send_create_signal(u'geo', ['Location'])


    def backwards(self, orm):
        # Deleting model 'Country'
        db.delete_table(u'geo_country')

        # Deleting model 'City'
        db.delete_table(u'geo_city')

        # Deleting model 'Location'
        db.delete_table(u'geo_location')


    models = {
        u'geo.city': {
            'Meta': {'object_name': 'City'},
            'country': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'cities'", 'to': u"orm['geo.Country']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'slug': ('autoslug.AutoSlugField', [], {'populate_from': "('name',)", 'unique': 'True', 'max_length': '50', 'db_index': 'False', 'blank': 'True'})
        },
        u'geo.country': {
            'Meta': {'object_name': 'Country'},
            'code': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '2'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'geo.location': {
            'Meta': {'ordering': "('name',)", 'object_name': 'Location'},
            'city': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'locations'", 'null': 'True', 'to': u"orm['geo.City']"}),
            'country': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'locations'", 'to': u"orm['geo.Country']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'keywords': ('django.db.models.fields.CharField', [], {'max_length': '1024', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'point': ('location_field.models.LocationField', [], {'null': 'True', 'blank': 'True'}),
            'rank': ('django.db.models.fields.IntegerField', [], {'default': '0', 'db_index': 'True'}),
            'state': ('localflavor.au.models.AUStateField', [], {'max_length': '3'}),
            'postcode': ('localflavor.au.models.AUPostCodeField', [], {'max_length': '4'})
        }
    }

    complete_apps = ['geo']
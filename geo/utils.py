import math
from urllib import urlencode
from django.contrib.gis.geos import Point
from django.conf import settings


EARTH = 6371.0
RAD = math.pi / 180.0
DEG = 180.0 / math.pi


def offset(point, km):
    "Returns the x, y offset in degrees for the distance km at the given point"
    r = EARTH * math.cos(point.y*RAD)
    x = (km / r)
    y = (km / EARTH)
    return x*DEG, y*DEG


def buffer_extent(point, km):
    off = offset(point, km)
    return point.x - off[0], point.y - off[1], point.x + off[0], point.y + off[1]


MAP_URL = 'http://maps.googleapis.com/maps/api/staticmap?%s&' % urlencode({
    'sensor': 'false',
    'key': getattr(settings, 'GOOGLE_API_KEY', "DEFAULT_KEY")
})


def get_static_map(point, zoom=12):
    if point == Point(0, 0):
        return
    params = {
        'markers': 'color:white|%s,%s' % (point.y, point.x),
        'center': '%s,%s' % (point.y, point.x),
        'zoom': zoom,
    }
    return MAP_URL + urlencode(params)


def does_state_code_exist(state_code):
    """
    does_state_code_exist("Act") => True
    does_state_code_exist("Actt") => False
    """
    from localflavor.au.au_states import STATE_CHOICES

    for code, name in STATE_CHOICES:
        if code.upper() == state_code.upper():
            return True

    return False
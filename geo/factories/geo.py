import factory
from models import Country, City, Location

__author__ = 'Francois CONSTANT'


class CountryFactory(factory.DjangoModelFactory):
    FACTORY_FOR = Country

    name = factory.Sequence(lambda n: 'country {0}'.format(n))
    code = factory.Sequence(lambda n: '{0}'.format(n))


class CityFactory(factory.DjangoModelFactory):
    FACTORY_FOR = City

    country = factory.SubFactory(CountryFactory)
    name = factory.Sequence(lambda n: 'city {0}'.format(n))
    #slug = factory.Sequence(lambda n: 'city-{0}'.format(n))


class LocationFactory(factory.DjangoModelFactory):
    FACTORY_FOR = Location

    country = factory.SubFactory(CountryFactory)
    city = None
    name = factory.Sequence(lambda n: 'location {0}'.format(n))
    state = "NSW"
    keywords = ""
    point = "0101000020E6100000F085C954C1E66240AE47E17A14EE40C0"
    rank = 0
from django import template
from localflavor.au.au_states import STATE_CHOICES

register = template.Library()
from geo.models import City


@register.assignment_tag
def menu_cities(count=20):
    return City.objects.order_by('name')[0:count]


@register.assignment_tag
def australian_states_listing():
    return STATE_CHOICES


@register.filter
def state_name(state_code):
    """
    ex: get_state_name("nsw") => "New South Wales"
    """
    for code, name in STATE_CHOICES:
        if code.upper() == state_code.upper():
            return name
    return ""
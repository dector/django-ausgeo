import os

from setuptools import setup, find_packages


here = os.path.abspath(os.path.dirname(__file__))
README = open(os.path.join(here, "README.md")).read()
# CHANGES = open(os.path.join(here, "CHANGES.md")).read()

install_requires = [
    'django_localflavor',
    'geopy',
    'django-autoslug',
    'factory_boy',
]

classifiers = [
    "Development Status :: 4 - Beta",
    "Framework :: Django",
    "License :: OSI Approved :: MIT License",
    "Programming Language :: Python",
    "Topic :: Software Development :: Libraries :: Python Modules"
]

setup(name='django-ausgeo',
      version='0.1',
      description='Aus Geo App',
      long_description="\n" + README + "\n\n", # + CHANGES,
      author='Rich Atkinson',
      author_email='rich@piran.com.au',
      license='MIT',
      download_url='https://github.com/',
      url='https://bitbucket.org/piran/django-ausgeo',
      include_package_data=True,
      zip_safe=False,
      classifiers=classifiers,
      install_requires=install_requires,
      packages=find_packages()
      )

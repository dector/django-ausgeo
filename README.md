Notes
=====
Geo tools with Australian data

initial version that isn't hacked in lib

Todo
----
Consider replacing concrete models + migrations with just AbstractBase


Requires
--------
A spacial template eg:

> https://docs.djangoproject.com/en/dev/ref/contrib/gis/install/postgis/

A location field eg:

> pip install -e git://github.com/litchfield/django-location-field.git#egg=django-location-field


License
-------

MIT